import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  todos: state.todos,
});

class DetailPage extends Component {
  render() {
    const {
      Country_Region,
      Last_Update,
      Lat,
      Long_,
      Confirmed,
      Deaths,
      Recovered,
      Active,
    } = this.props.route.params;
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.contentText}>Negara:</Text>
          <Text style={styles.contentText2}>{Country_Region}</Text>
          <Text style={styles.contentText}></Text>
          <Text style={styles.contentText}>Positif:</Text>
          <Text style={styles.contentText2}>{Confirmed}</Text>
          <Text style={styles.contentText}></Text>
          <Text style={styles.contentText}>Sembuh:</Text>
          <Text style={styles.contentText2}>{Recovered}</Text>
          <Text style={styles.contentText}></Text>
          <Text style={styles.contentText}>Meninggal:</Text>
          <Text style={styles.contentText2}>{Deaths}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
    paddingHorizontal: "15%",
    paddingVertical: "20%",
    backgroundColor: "#CECECE",
  },
  content: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: "10%",
    backgroundColor: "#fff",
  },
  contentText: {
    fontSize: 18,
    fontWeight: "bold",
  },
  contentText2: {
    fontSize: 18,
  },
});

export default connect(mapStateToProps)(DetailPage);
