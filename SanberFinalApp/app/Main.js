import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
//import "react-native-gesture-handler";

import LoginPage from "./LoginPage";
import ProfilePage from "./ProfilePage";
import HomePage from "./HomePage";
import DetailPage from "./DetailPage";

import TabBarIcon from "./components/TabBarIcon";

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeRoot = createStackNavigator();

const HomeRootScreen = () => (
  <HomeRoot.Navigator>
    <HomeRoot.Screen
      name="Home"
      component={HomePage}
      options={{ headerShown: false }}
    />
    <HomeRoot.Screen name="Detail" component={DetailPage} />
  </HomeRoot.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen
      name="HomeRoot"
      component={HomeRootScreen}
      options={{
        title: "Beranda",
        tabBarIcon: ({ focused }) => (
          <TabBarIcon focused={focused} name="ios-home" />
        ),
      }}
    />
    <Tabs.Screen
      name="Profile"
      component={ProfilePage}
      options={{
        title: "Profil",
        tabBarIcon: ({ focused }) => (
          <TabBarIcon focused={focused} name="ios-person" />
        ),
      }}
    />
  </Tabs.Navigator>
);

export default function Main() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={LoginPage}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Tabs"
          component={TabsScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
