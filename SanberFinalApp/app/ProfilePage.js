import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  todos: state.todos,
});

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: this.props.todos[0],
      Password: this.props.todos[1],
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerBox}>
            <Text>Halo, {this.state.username}</Text>
          </View>
        </View>
        <View style={styles.content}>
          <View style={styles.contentBox}>
            <Text style={styles.contentText}>Password:</Text>
            <Text style={styles.contentText2}>{this.state.Password}</Text>
            <Text style={styles.contentText}></Text>
            <Text style={styles.contentText}>Alamat:</Text>
            <Text style={styles.contentText2}>
              Jalan Perjuangan Utama No. 15 Surabaya
            </Text>
            <Text style={styles.contentText}></Text>
            <Text style={styles.contentText}>No HP:</Text>
            <Text style={styles.contentText2}>081 123 456 789</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Login")}
            style={styles.logoutButton}
          >
            <Text style={styles.logoutText}>LOGOUT</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
    paddingHorizontal: "5%",
    backgroundColor: "#CECECE",
  },
  header: {
    flex: 1,
    height: 50,
    width: "100%",
    paddingTop: 50,
    alignSelf: "center",
    //backgroundColor: "#fff012",
  },
  headerBox: {
    backgroundColor: "#fff",
    padding: "5%",
    borderRadius: 10,
  },
  content: {
    flex: 8,
    paddingTop: 30,
  },
  contentBox: {
    backgroundColor: "#fff",
    padding: "5%",
    borderRadius: 10,
    marginBottom: 10,
  },
  contentText: {
    fontSize: 14,
    fontWeight: "bold",
  },
  contentText2: {
    fontSize: 14,
  },
  logoutButton: {
    padding: 10,
    backgroundColor: "#7B7B7B",
    borderRadius: 10,
    alignItems: "center",
  },
  logoutText: {
    color: "white",
  },
});

export default connect(mapStateToProps)(ProfilePage);
