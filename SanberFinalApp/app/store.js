import { createStore } from "redux";
import { reducer } from "./reduxUser";

// Mendefinisikan store menggunakan reducer
const store = createStore(reducer);

export default store;
