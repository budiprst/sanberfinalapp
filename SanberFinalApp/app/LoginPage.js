import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Input,
  TextInput,
  Image,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import { actionCreators } from "./reduxUser";
import Colors from "./components/Colors";

const mapStateToProps = (state) => ({
  todos: state.todos,
});

class LoginPage extends Component {
  state = {
    textUser: "",
    textPass: "",
  };

  onChangeTextUser = (textUser) => this.setState({ textUser });
  onChangeTextPass = (textPass) => this.setState({ textPass });
  onSubmitEditing = async ({ navigation }) => {
    const { onSubmitEditing } = this.props;
    const { textUser, textPass } = this.state;

    if (!textUser && !textPass) return; // Don't submit if empty

    //onSubmitEditing(text);
    await this.onRemoveTodo(0);
    console.log("After onRemoveTodo");

    await this.onAddTodo(textUser, textPass);
    //console.log("After onAddTodo");
    this.setState({ textUser: "", textPass: "" });
    this.props.navigation.navigate("Tabs", {
      screen: "Home",
    });
  };

  onAddTodo = async (textUser, textPass) => {
    console.log("User txt input: ", textUser);
    console.log("Pass txt input: ", textPass);

    const { dispatch } = this.props;

    await dispatch(actionCreators.add(textPass));
    await dispatch(actionCreators.add(textUser));
  };

  onRemoveTodo = async (index) => {
    const { dispatch } = this.props;

    await dispatch(actionCreators.remove(index));
    await dispatch(actionCreators.remove(index));
  };

  render() {
    const { textUser, textPass } = this.state;
    const { todos } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.logoCont}>
          <View style={styles.imgCont}>
            <Image
              source={require("../assets/corona_header.jpg")}
              style={styles.logo}
            />
          </View>
          <View style={styles.formTitle}>
            <Text style={styles.title}>Login</Text>
          </View>
        </View>
        <View style={styles.formCont}>
          <View style={styles.formContent}>
            <Text style={styles.formContentTitle}>Username/Email</Text>
            <TextInput
              style={styles.formContentInput}
              onChangeText={this.onChangeTextUser}
              value={textUser}
            />
          </View>
          <View style={styles.formContent}>
            <Text style={styles.formContentTitle}>Password</Text>
            <TextInput
              style={styles.formContentInput}
              onChangeText={this.onChangeTextPass}
              value={textPass}
              secureTextEntry
            />
          </View>
        </View>
        <View style={styles.footerCont}>
          <TouchableOpacity
            style={styles.buttonCont}
            onPress={() => this.onSubmitEditing(this.props.navigation)}
          >
            <View style={styles.buttonMasuk}>
              <Text style={styles.buttontext}>Masuk</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
  },
  logoCont: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  formCont: {
    flex: 0.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    paddingHorizontal: "10%",
    paddingVertical: "5%",
  },
  footerCont: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 30,
  },

  imgCont: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "20%",
  },
  logo: {
    height: 100,
    width: 250,
    resizeMode: "cover",
    backgroundColor: "#123fff",
    borderWidth: 1,
  },
  formTitle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#7B7B7B",
  },
  formContent: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
  },
  formContentTitle: {
    fontSize: 12,
    fontWeight: "bold",
    color: "#000",
  },
  formContentInput: {
    width: "100%",
    height: "45%",
    borderWidth: 1,
    borderColor: "#7B7B7B",
  },
  buttonCont: {
    alignItems: "center",
    width: "35%",
  },
  buttonMasuk: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: 30,
    //borderWidth: 1,
    //borderColor: Colors.darkBlue,
    borderRadius: 12,
    backgroundColor: "#7B7B7B",
  },
  buttontext: {
    color: "#fff",
  },
});

export default connect(mapStateToProps)(LoginPage);
