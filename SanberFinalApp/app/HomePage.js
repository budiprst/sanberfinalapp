import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import { connect } from "react-redux";

import axios from "axios";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";

const mapStateToProps = (state) => ({
  todos: state.todos,
});

class HomePage extends Component {
  constructor(props) {
    super(props);
    prefik_url = "https://api.kawalcorona.com";
    this.state = {
      categories: [
        {
          attributes: {
            OBJECTID: 59,
            Country_Region: "China",
            Last_Update: 1584097775000,
            Lat: 30.5928,
            Long_: 114.3055,
            Confirmed: 81346,
            Deaths: 3265,
            Recovered: 72355,
            Active: 5726,
          },
        },
      ],
      username: this.props.todos[0],
    };
  }

  getDataCorona = () => {
    axios.get(prefik_url).then((res) => {
      const categories = res.data;
      //console.log(categories);
      this.setState({ categories });
      //console.log(this.state.categories);
    });
  };
  componentDidMount() {
    this.getDataCorona();
  }

  render() {
    const { categories } = this.state;
    //console.log(categories);

    const renderItem = ({ item }) => (
      <TouchableOpacity
        key={item.Country_Region}
        onPress={() =>
          this.props.navigation.push("Detail", {
            Country_Region: item.attributes.Country_Region,
            Last_Update: item.attributes.Last_Update,
            Lat: item.attributes.Lat,
            Long_: item.attributes.Long_,
            Confirmed: item.attributes.Confirmed,
            Deaths: item.attributes.Deaths,
            Recovered: item.attributes.Recovered,
            Active: item.attributes.Active,
          })
        }
      >
        <View key={item.Country_Region} style={styles.listContainer}>
          <View style={styles.listCountry}>
            <Text>{item.attributes.Country_Region}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerBox}>
            <Text>Halo, {this.state.username}</Text>
          </View>
        </View>
        <View style={styles.content}>
          <Text style={styles.contentText}>Daftar Negara</Text>
          <FlatList
            data={categories}
            renderItem={renderItem}
            keyExtractor={(item) => item.Country_Region}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
    paddingHorizontal: "5%",
    backgroundColor: "#CECECE",
  },
  header: {
    flex: 1,
    height: 50,
    width: "100%",
    paddingTop: 50,
    alignSelf: "center",
    //backgroundColor: "#fff012",
  },
  headerBox: {
    backgroundColor: "#fff",
    padding: "5%",
    borderRadius: 10,
  },
  content: {
    flex: 8,
    paddingTop: 30,
  },
  contentText: {
    paddingHorizontal: "5%",
    paddingBottom: 10,
  },
  listContainer: {
    padding: 5,
  },
  listCountry: {
    justifyContent: "center",
    paddingHorizontal: "5%",
    backgroundColor: "#fff",
    height: 50,
    borderRadius: 10,
  },
});

export default connect(mapStateToProps)(HomePage);
