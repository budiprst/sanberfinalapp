import React from "react";
import { View, Text } from "react-native";
//import { createStore } from "redux";
import { Provider } from "react-redux";

// Import komponen App
//import Main from "./redux/Main";
import Main from "./app/Main";
//import Main from "./app/index/index";

// Import store
import store from "./app/store";

// Memasukkan store pada Provider
export default function App() {
  return (
    <Provider store={store}>
      <Main />
    </Provider>
  );
}

/* 
    
    <View>
      <Text>Ini coba saja</Text>
    </View>
    */
